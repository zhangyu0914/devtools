package com.devtools.generator.service;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.generator.FastAutoGenerator;
import com.baomidou.mybatisplus.generator.IFill;
import com.baomidou.mybatisplus.generator.config.OutputFile;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;
import com.baomidou.mybatisplus.generator.engine.VelocityTemplateEngine;
import com.baomidou.mybatisplus.generator.fill.Column;
import com.devtools.generator.task.GenerateTask;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author zhangyu
 **/
@Service
public class GenerateService {

    @Value("${spring.datasource.url}")
    private String url;

    @Value("${spring.datasource.username}")
    private String username;

    @Value("${spring.datasource.password}")
    private String password;

    /**
     * 生成代码
     */
    public void generate(GenerateTask generateTask) {
        List<IFill> columns = new ArrayList<>();
        columns.add(new Column("create_time", FieldFill.INSERT));
        columns.add(new Column("update_time", FieldFill.INSERT_UPDATE));

        FastAutoGenerator.create(url + "/" + generateTask.getDbName(), username, password)
                .globalConfig(builder -> {
                    builder.author(generateTask.getDeveloper()) // 设置作者
                            .enableSwagger() // 开启 swagger 模式
                            .fileOverride() // 覆盖已生成文件
                            .outputDir(generateTask.getProjectLocation())
                    ; // 指定输出目录
                })
                .packageConfig(builder -> {
                    Map<OutputFile, String> map = new HashMap<>();
                    map.put(OutputFile.mapperXml, generateTask.getProjectLocation() + "/xml");// 设置mapperXml生成路径
                    builder.parent(generateTask.getPackageName()) // 设置controller生成路径
                            .controller("web")
                            .pathInfo(map);
                })
                .strategyConfig(builder ->
                        builder.addInclude(generateTask.getTableNames()) // 设置需要生成的表名
                                .addTablePrefix("ques")
                                .entityBuilder()
                                .enableLombok()
                                .enableChainModel()
                                .enableRemoveIsPrefix()
                                .enableTableFieldAnnotation()
                                .naming(NamingStrategy.underline_to_camel)
                                .addTableFills(columns)
                                .idType(IdType.ASSIGN_ID)
                                .controllerBuilder()
                                .formatFileName("%sController")
                                .enableRestStyle()
                                .mapperBuilder()
                                .formatMapperFileName("%sMapper")
                                .enableMapperAnnotation()
                                .enableBaseResultMap()
                                .formatXmlFileName("%sMapper")
                                .serviceBuilder()
                                .formatServiceFileName("I%sService")
                                .formatServiceImplFileName("%sServiceImpl"))
                .templateEngine(new VelocityTemplateEngine()) // 使用Freemarker引擎模板，默认的是Velocity引擎模板
                .execute();
    }

}
