package com.devtools.generator.controller;

import com.devtools.generator.service.GenerateService;
import com.devtools.generator.service.TableService;
import com.devtools.generator.task.GenerateTask;
import com.devtools.generator.properties.DevtoolsProperties;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

/**
 * 代码生成控制器
 *
 * @author zhangyu
 * @Date 2020-01-14
 */
@RestController
@Api(value = "/generator", tags = "代码生成器接口")
@RequestMapping("/generator")
public class GeneratorController {

    @Resource
    private TableService tableService;

    @Resource
    private DevtoolsProperties devtoolsProperties;

    @Resource
    private GenerateService generateService;

    /**
     * 获取环境信息
     */
    @GetMapping("/env")
    @ApiOperation("获取代码生成器配置")
    public Object blackboard() {
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("tables", tableService.getAllTables());
        hashMap.put("params", devtoolsProperties);
        Map<String, Object> map = new HashMap<>();
        map.put("success", true);
        map.put("code", 0);
        map.put("data", hashMap);
        map.put("msg", "操作成功");
        return map;
    }

    /**
     * 生成代码
     */
    @ApiOperation("生成代码")
    @PostMapping("/generate")
    public Object generate(@RequestBody GenerateTask generateTask) {
        generateService.generate(generateTask);
        Map<String, Object> map = new HashMap<>();
        map.put("success", true);
        map.put("code", 0);
        map.put("msg", "操作成功");
        return map;
    }
}
