package com.devtools.generator.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 代码生成配置
 *
 * @author zhangyu
 * @date 2020-01-13
 */
@Data
@Component
@ConfigurationProperties(prefix = "devtools")
public class DevtoolsProperties {

    /**
     * 项目路径
     */
    private String projectLocation;

    /**
     * 数据库
     */
    private List<String> dbNames;

    /**
     * 开发者
     */
    private List<String> devNames;
}
