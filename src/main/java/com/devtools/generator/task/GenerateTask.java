package com.devtools.generator.task;

import lombok.Data;

/**
 * @author zhangyu
 * @date 2020-01-14
 **/
@Data
public class GenerateTask {

    /**
     * 项目路径
     */
    private String projectLocation;

    /**
     * 包名
     */
    private String packageName;

    /**
     * 数据库表
     */
    private String[] tableNames;

    /**
     * 数据库表名
     */
    private String[] entityNames;

    /**
     * 开发者
     */
    private String developer;

    /**
     * 数据库名称
     */
    private String dbName;

}
